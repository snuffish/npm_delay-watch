# delay-watch

[![build status](https://img.shields.io/travis/com/snuffish/delay-watch.svg)](https://travis-ci.com/snuffish/delay-watch)
[![code coverage](https://img.shields.io/codecov/c/github/snuffish/delay-watch.svg)](https://codecov.io/gh/snuffish/delay-watch)
[![code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/sindresorhus/xo)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![made with lass](https://img.shields.io/badge/made_with-lass-95CC28.svg)](https://lass.js.org)
[![license](https://img.shields.io/github/license/snuffish/delay-watch.svg)](LICENSE)

> my kryptonian project


## Table of Contents

* [Install](#install)
* [Usage](#usage)
* [Contributors](#contributors)
* [License](#license)


## Install

[npm][]:

```sh
npm install delay-watch
```

[yarn][]:

```sh
yarn add delay-watch
```


## Usage

```js
const DelayWatch = require('delay-watch');

const delayWatch = new DelayWatch();

console.log(delayWatch.renderName());
// script
```


## Contributors

| Name         |
| ------------ |
| **Snuffish** |


## License

[MIT](LICENSE) © Snuffish


## 

[npm]: https://www.npmjs.com/

[yarn]: https://yarnpkg.com/
