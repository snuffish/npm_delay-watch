import fetch from 'node-fetch'
import { TrainTrip } from './src/TrainTrip'
import StationConnection from './src/StationConnection'
import Endpoint from './src/Endpoint'
import printf from 'printf'
import { LocationCode, getLocation } from './src/LocationCode'
import { DATE_FORMAT, getDate } from './src/utils/DateUtils'
import { getJsonFromEndpoint } from './src/utils/RequestUtil'
import { getStationConnections } from './src/utils/TrainUtils'
import { createBuilderStatusReporter } from 'typescript'
import StationsData from './src/data/Stations.data'
import { JsonResponse, ITripResponse, IStation, IConnection } from './src/types'
import { createIndexSignature } from 'typescript'
import { access } from 'fs'





let str: string | null = 'bar'
str = 123
console.log(str)




;(async() => {

  const location: LocationCode = getLocation('G')
  const station = new StationConnection(location)
  const stationResponse: JsonResponse = await station.execute()

  if (stationResponse.Success) {
    const res: LocationCode = stationResponse.DepartureConnections[0].DepartureLocationCode
    //console.log(res)

  }


  //writeFileSync('/Users/chreng/enumdump.txt', str)

  /*const gothenburg = new StationConnection(LocationCode.GÖTEBORG_C)
  const data = await gothenburg.execute()
  console.log(data)*/



  /*const trainTrips: TrainTrip[] = []

  const trainIDs = await getStationConnections(LocationCode.GÖTEBORG_C)
  for (const trainId of trainIDs) {
    const trip = new TrainTrip(trainId)
    const tripResponse = await trip.execute()
    if (tripResponse.Success) {
      trainTrips.push(trip)
    }
  }*/

  /*const trainIds = await getStationConnections(LocationCode.GÖTEBORG_C)
  for (const trainId of trainIds) {
    const trainTrip = new TrainTrip(trainId)
    const executeRes = await trainTrip.execute()
    if (executeRes.Success) {
      console.log("", trainTrip.operator)
      console.log("delayedStations => ", trainTrip.delayedStations.length)
    }
  }*/

})()
