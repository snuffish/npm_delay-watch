import Endpoint from '../Endpoint'
import printf from 'printf'
import fetch from 'node-fetch'
import { EndpointOptions, JsonResponse, Url } from '../types'

export const getJsonFromEndpoint = async (endpoint: Endpoint, options: EndpointOptions): Promise<JsonResponse> => {
  let url: Url
  if (endpoint === Endpoint.TRAFFIC_URL) {
    url = printf(Endpoint.TRAFFIC_URL, options[0], options[1])
  } else if (endpoint === Endpoint.TRAFFIC_JSON) {
    url = printf(Endpoint.TRAFFIC_JSON, options[0])
  } else if (endpoint === Endpoint.STATION_CONNECTIONS_JSON) {
    url = printf(Endpoint.STATION_CONNECTIONS_JSON, options[0], options[1])
  }

  if (url) {
    const req = await fetch(url)
    const res = await req.text()
    return convertToJsonResponse(res)
  }

  return {} as JsonResponse
}

const convertToJsonResponse = (data: string): JsonResponse  => {
  data = data.replace('sj.apiProxy.jsonpCallbackMiocStationConnections(', '')
  .replace('sj.apiProxy.jsonpCallbackMiocTrainRoute(', '')
  .replace(');', '')

  return JSON.parse(data)
}
