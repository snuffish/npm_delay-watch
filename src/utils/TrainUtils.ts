import StationsData from '../data/Stations.data'
import Endpoint from '../Endpoint'
import { LocationCode } from '../LocationCode'
import { getJsonFromEndpoint } from './RequestUtil'
import { DATE_FORMAT, getDate } from '../utils/DateUtils'
import { IConnectionResponse } from '../types'

export const getStationConnections = async (locationCode: LocationCode): Promise<number[]> => {
  const trainTrips: IConnectionResponse = await getJsonFromEndpoint(Endpoint.STATION_CONNECTIONS_JSON,[locationCode, getDate(DATE_FORMAT.SJ)]) as IConnectionResponse

  if (trainTrips.Success && trainTrips.DepartureConnections) {
    return trainTrips.DepartureConnections.map(train => parseInt(train.AnnouncedTrainNumber))
  }

  return []
}

export const getStationName = (locationCode: string): string => {
  const fetchItem = StationsData.filter(item => item.id === locationCode).pop()
  return fetchItem !== undefined ? fetchItem.name : ''
}
