import moment from 'moment'

export enum DATE_FORMAT {
  SJ = 'YYYY-MM-DD+hh:mm',
  DATE = 'YYYY-MM-DD',
  DATETIME = "YYYY-MM-DD hh:mm:ss"
}

export const getDate = (format: DATE_FORMAT) => {
  let date = moment().format(format)
  if (format === DATE_FORMAT.SJ) date = date.replace(':', '%3A')
  return date
}

export const timeDifference = (startTime: string, stopTime: string): number => {
  if (startTime === '' || stopTime === '') return 0

  const start = moment(`${ getDate(DATE_FORMAT.DATE) } ${ startTime }`)
  const stop = moment(`${ getDate(DATE_FORMAT.DATE) } ${ stopTime }`)
  const msDiff: any = stop.diff(start).toFixed()

  return msDiff / 60000
}
