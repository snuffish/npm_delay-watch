enum Endpoint {
  TRAFFIC_URL = 'https://www2.sj.se/sv/trafikinfo/trafiken-idag.html/search/%s/Date/%s',
  TRAFFIC_JSON = 'https://services.trafficinfo.sj.se/v4.2/traffic/TrainRoute?callback=sj.apiProxy.jsonpCallbackMiocTrainRoute&AnnouncedTrainNumber=%s&FilterCode=DEFAULT',
  STATION_CONNECTIONS_JSON = 'https://services.trafficinfo.sj.se/v4.2/traffic/StationConnections?callback=sj.apiProxy.jsonpCallbackMiocStationConnections&LocationCode=%s&DateTime=%s&FilterCode=DEFAULT'
}

export default Endpoint
