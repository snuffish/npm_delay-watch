import printf from 'printf'
import Endpoint from './Endpoint'
import { DATE_FORMAT, getDate, timeDifference } from './utils/DateUtils'
import { getJsonFromEndpoint } from './utils/RequestUtil'
import { IStation, ExecuteResponse, ITripResponse, ILocation, JsonResponse } from './types'
import { getStationName } from './utils/TrainUtils'

export class TrainTrip {
  private SiteUrl: string
  private JsonUrl: string
  private Operator: string

  private StartLocation: ILocation = {}
  private FinalLocation: ILocation = {}

  private Stations: Array<IStation> = []
  private DelayedStations: Array<IStation> = []

  constructor(private TrainNumber: number, private DelayedMinutes: number = 3) {
    this.SiteUrl = printf(Endpoint.TRAFFIC_URL, this.TrainNumber, getDate(DATE_FORMAT.DATE))
  }

  public execute = async (): Promise<ExecuteResponse> => {
    const trip: ITripResponse = await this.getTrainJson() as ITripResponse
    if (!trip.Success) {
      const { ErrorCodeText, ErrorCodeValue, Success } = trip
      return { ErrorCodeText, ErrorCodeValue, Success }
    }

    this.Operator = trip.Operator
    this.StartLocation.LocationCode = trip.Stations[0].LocationCode
    this.StartLocation.LocationName = getStationName(this.StartLocation.LocationCode)

    const finalStation = trip.Stations[trip.Stations.length - 1]
    this.FinalLocation.LocationCode = finalStation.LocationCode
    this.FinalLocation.LocationName = getStationName(this.FinalLocation.LocationCode)

    this.Stations = trip.Stations
    for (const station of this.Stations) {
      const { Time, RealTime } = station.Departure
      const minDiff = timeDifference(Time, RealTime)
      if (minDiff >= this.DelayedMinutes) this.DelayedStations.push(station)
    }

    return { Success: true }
  }

  private getTrainJson = async (): Promise<JsonResponse> => await getJsonFromEndpoint(Endpoint.TRAFFIC_JSON, [this.TrainNumber])
}
