import Endpoint from  './Endpoint'
import printf from 'printf'
import { LocationCode } from './LocationCode'
import { getJsonFromEndpoint } from './utils/RequestUtil'
import { DATE_FORMAT, getDate } from './utils/DateUtils'

export default class StationConnection {
  private JsonUrl: string

  constructor(private LocationCode: LocationCode) {
    this.JsonUrl = printf(Endpoint.STATION_CONNECTIONS_JSON, this.LocationCode, getDate(DATE_FORMAT.SJ))
  }

  public execute = async () => await getJsonFromEndpoint(Endpoint.STATION_CONNECTIONS_JSON, [this.LocationCode, getDate(DATE_FORMAT.SJ)])

  get jsonUrl(): string {
    return this.JsonUrl
  }
}
