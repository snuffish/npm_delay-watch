import { LocationCode } from ".."

export type EMPTY_STRING = ''
export type Url = string | undefined
export type EndpointOptions = string[] | number[] | EMPTY_STRING
export type JsonResponse = ITripResponse & IConnectionResponse

export type KeyValuePair<Key extends string, Type> = { [k in Key]: Type }

export type LongLat = { Latitude: string, Longitude: string }
export type GpsData = { LastUpdatedDateTime: string, Speed: string } & LongLat

export type BasicResponse = { ErrorCodeText: string, ErrorCodeValue: number, Success: boolean }

export interface IConnectionResponse extends BasicResponse {
  LocationCode: LocationCode,
  AnnouncedTrainNumber: string,
  ArrivalConnections: Array<IConnection>,
  DepartureConnections: Array<IConnection>,
  GpsData: GpsData
}

export interface ITripResponse extends BasicResponse {
  AnnouncedTrainNumber: number,
  StartDepartureLocationCode: LocationCode,
  FinalDestinationLocationCode: LocationCode,
  Operator: string,
  InformationOwner: string,
  Stations: Array<IStation>
}

export interface IConnection {
  AnnouncedTrainNumber: string,
  DepartureLocationCode: LocationCode,
  DestinationLocationCode: LocationCode,
  Operator: string,
  Time: string, // Announced time
  StatusTime: string // Real time
  Track: number,
  GpsData: GpsData
}

export interface IStation {
  LocationCode: LocationCode,
  Arrival: IStationInfo,
  Departure: IStationInfo,
  IsDelayed: boolean,
  IsMarkDelayed: boolean,
  HasArrived: boolean,
  HasDeparted: boolean
}

export interface IStationInfo {
  Time: string, // Announced time
  RealTime: string, // Real time
  IsDelayed: boolean,
  IsCancelled: boolean
}

export interface ILocation {
  LocationCode?: LocationCode,
  LocationName?: string
}

export interface ExecuteResponse {

}
